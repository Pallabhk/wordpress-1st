<?php get_header();?>
          
    <div class="row container-kamn">
        <img src="assets/img/slider/slide5.jpg" class="blog-post" alt="Feature-img" align="right" width="100%"> 
    </div>

    <!-- End Header -->


    <!-- Main Container -->
    <div id="banners"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <?php
                    $office_post =null;
                    $office_post =new WP_Query(array(
                        'post_type'=>'team',
                        'posts_per_page'=>-1
                    ));
                    if($office_post->have_posts()){
                        while($office_post->have_posts()){
                            $office_post->the_post();
                            $team_designation = get_post_meta(get_the_ID(),'_office-master_team_designation',true);

                            ?>
                            <div class="blog-post">
                                <h1 class="blog-title">
                                    <i class="fa fa-file-text"></i>
                                    <?php the_title();?>
                                </h1>
                                <br>
                                <?php the_post_thumbnail();?>
                                <br>
                                <?php the_excerpt();?>
                                <div>
                                    <span class="badge">Posted <?php gert_the_date('Y-m-d H:m:s')?></span>
                                    <div class="pull-right">
                                        <span class="label label-default">alice</span>
                                        <span class="label label-primary">story</span>
                                        <span class="label label-success">blog</span>
                                        <span class="label label-info">personal</span>
                                        <span class="label label-warning">Warning</span>
                                        <span class="label label-danger">Danger</span>
                                    </div>
                                </div>
                            </div>
                            <hr>

                        <?php }

                    }else{
                        echo 'No post';
                    }
                    wp_reset_postdata();

                    ?>

                 <ul class="pagination">
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">2 <span class="sr-only"></span></a></li>
                        <li><a href="#">3 <span class="sr-only"></span></a></li>
                        <li><a href="#">4 <span class="sr-only"></span></a></li>
                        <li><a href="#">5 <span class="sr-only"></span></a></li>
                    </ul>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Sign in </strong></h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Username or Email</label>
                                    <input type="email" class="form-control" style="border-radius:0px" id="exampleInputEmail1" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password <a href="/sessions/forgot_password">(forgot password)</a></label>
                                    <input type="password" class="form-control" style="border-radius:0px" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-sm btn-default">Sign in</button>
                            </form>
                        </div>
                    </div>

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="http://placehold.it/292/16a085/FFF&text=CSS3" alt="" class="img-responsive" />
                            </div>
                            <div class="item">
                                <img src="http://placehold.it/292/d35400/FFF&text=HTML5" alt="" class="img-responsive" />
                            </div>
                            <div class="item">
                                <img src="http://placehold.it/292/2980b9/FFF&text=RESPONSIVE" alt="" class="img-responsive" />
                            </div>
                            <div class="item">
                                <img src="http://placehold.it/292/8e44ad/FFF&text=BOOTSTRAP3" alt="" class="img-responsive" />
                            </div>
                        </div>
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
       
        <!--End Main Container -->
<?php get_footer();?>
