<?php
/**
 * Get the bootstrap!
 */
if ( file_exists( __DIR__ . '/cmb2/init.php' ) ) {
    require_once __DIR__ . '/cmb2/init.php';
} elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
    require_once __DIR__ . '/CMB2/init.php';
}
add_action( 'cmb2_admin_init', 'office_master_cmb2' );

function office_master_cmb2(){
            $prief='_office-master_';
        $service_item = new_cmb2_box( array(
            'id'            => 'service_metabox',
            'title'         => __( 'Service Metabox', 'office_master' ),
            'object_types'  => array( 'services', ), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left

        ) );
            $service_item->add_field( array(
                'name'       => __( 'Services-Icon', 'Office_master' ),
                'desc'       => __( 'write your service icon fontawsome  name', 'Office_master' ),
                'id'            => $prief.'service_icon',
                'type'       => 'text',
                'repeatable' =>true


            ) );
                $service_item->add_field( array(
                    'name'       => __( 'Services-Description', 'Office_master' ),
                    'desc'       => __( 'write your service Description', 'Office_master' ),
                    'id'            => $prief.'service_description',
                    'type'       => 'textarea'


                ) );
                $service_item->add_field( array(
                    'name'       => __( 'Services-link-url', 'Office_master' ),
                    'desc'       => __( 'write your service link url', 'Office_master' ),
                    'id'            => $prief.'service_link_url',
                    'type'       => 'text'


                ) );
                $service_item->add_field( array(
                    'name'       => __( 'Services-link-title', 'Office_master' ),
                    'desc'       => __( 'write your service title', 'Office_master' ),
                    'id'            => $prief.'service_link_title',
                    'type'       => 'text'


                ) );
                $service_item->add_field( array(
                    'name'       => __( 'Services-Animation-type', 'Office_master' ),
                    'desc'       => __( 'write your service title', 'Office_master' ),
                    'id'         => $prief.'animation_type',
                    'type'       => 'text'


                ) );

                    $slider_item = new_cmb2_box( array(
                        'id'            => 'Slider_metabox',
                        'title'         => __( 'Slider Metabox', 'office_master' ),
                        'object_types'  => array( 'slider','services','page' ), // Post type
                        'context'       => 'normal',
                        'priority'      => 'high',
                        'show_names'    => true, // Show field names on the left

                    ) );

                         $slider_item->add_field( array(
                            'name'       => __( 'Slider-Caption', 'Office_master' ),
                            'desc'       => __( 'write your Slider Caption', 'Office_master' ),
                            'id'         => $prief.'slider_caption',
                            'type'       => 'text'


                        ) );
                        $special = new_cmb2_box( array(
                            'id'            => 'special_metabox',
                            'title'         => __( 'special Metabox', 'office_master' ),
                            'object_types'  => array( 'page' ), // Post type
                            'show_on'       =>array(
                                'key' =>'id',
                                'value'=>'76',
                            ),
                            'context'       => 'normal',
                            'priority'      => 'high',
                            'show_names'    => true, // Show field names on the left

                        ) );
                        $special->add_field( array(
                                'name'       => __( 'special-Caption', 'Office_master' ),
                                'desc'       => __( 'write your Slider Caption', 'Office_master' ),
                                'id'         => $prief.'special_page_caption',
                                'type'       => 'text'


                            ) );
                        $team_member = new_cmb2_box( array(
                            'id'            => 'Team_member_metabox',
                            'title'         => __( 'Team_member Metabox', 'office_master' ),
                            'object_types'  => array( 'team' ), // Post type
                            'context'       => 'normal',
                            'priority'      => 'high',
                            'show_names'    => true, // Show field names on the left

                        ) );
                              $team_member->add_field( array(
                                'name'       => __( 'Team_Member_Designation', 'Office_master' ),
                                'desc'       => __( 'write your Team member Designation', 'Office_master' ),
                                'id'         => $prief.'team_designation',
                                'type'       => 'text'


                            ) );
                            $team_member->add_field( array(
                                'name'       => __( 'Blockquote color ', 'Office_master' ),
                                'desc'       => __( 'write Blockquote color', 'Office_master' ),
                                'id'         => $prief.'block_color',
                                'type'       => 'text'


                            ) ); $team_member->add_field( array(
                                'name'       => __( 'Animation type', 'Office_master' ),
                                'desc'       => __( 'write  Animation type', 'Office_master' ),
                                'id'         => $prief.'animation_type_class',
                                'type'       => 'text'


                            ) );
}