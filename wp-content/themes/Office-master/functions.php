<?php

    function office_master_theme_support(){

        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_image_size('slide-img',1500,500,true);
        add_image_size('team-member',100,80,true);
        register_nav_menus(array(
            'primary_menu' =>'Primary Menu'
        ));
    }
    add_action('after_setup_theme','office_master_theme_support');

    function office_master_css_js(){

        wp_enqueue_style('google-font-1','//fonts.googleapis.com/css?family=Open+Sans:400,300','null','v1.0','all');
        wp_enqueue_style('google-font-2','//fonts.googleapis.com/css?family=PT+Sans','null','v1.0','all');
        wp_enqueue_style('google-font-3','//fonts.googleapis.com/css?family=Raleway','null','v1.0','all');
        wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/bootstrap/css/bootstrap.css');
        wp_enqueue_style('font-awesome',get_template_directory_uri().'/assets/css/font-awesome.min.css');
        wp_enqueue_style('style-main',get_template_directory_uri().'/assets/css/style.css');
        wp_enqueue_style('animate',get_template_directory_uri().'/assets/css/animate.min.css');
        wp_enqueue_style('office-master-main-style',get_stylesheet_uri());

        wp_enqueue_script('jquery');
        wp_enqueue_script('bootstrap',get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js','jquery',null,true);
        wp_enqueue_script('wow-js',get_template_directory_uri().'/js/wow.min.js','jquery',null,true);

    }
    add_action('wp_enqueue_scripts','office_master_css_js');

    function officer_extra_footer_script(){?>

        <script>
            new WOW().init();
        </script>
    <?php }
    add_action('wp_footer','officer_extra_footer_script',30);

        function office_master_fallback_menu(){?>
            <ul class="nav navbar-nav pull-right">
                <li class="active">
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Blog</a>
                </li>
                <li>
                    <a href="#">Team</a>
                </li>
                <li>
                    <a href="#"><span>Contact</span></a>
                </li>
            </ul>
    <?php }
    function office_master_custom_post(){

        register_post_type('slider',array(
            'labels'=>array(
                'name' =>'Main Slider',
                'menu_name'=>'Slider Menu',
                'all_items'=>'All Sliders',
                'add_new'=>'Add new Slide',
                'add_new_item'=>'Add new slide item'
            ),
            'public' =>true,
            'supports' =>array(
                'title','thumbnail','revisions','custom-fields','page-attributes'
            )
        ));
        register_post_type('services',array(
            'labels'=>array(
                'name' =>'Service',
                'menu_name'=>'Service Menu',
                'all_items'=>'All Service',
                'add_new'=>'Add new Service',
                'add_new_item'=>'Add new Service item'
            ),
            'public' =>true,
            'supports' =>array(
                'title','revisions','custom-fields','page-attributes'
            )
        ));
        register_post_type('team',array(
            'labels'=>array(
                'name' =>'Team',
                'menu_name'=>'Team Menu',
                'all_items'=>'All Team Member',
                'add_new'=>'Add new Team Member',
                'add_new_item'=>'Add new Team Member'
            ),
            'public' =>true,
            'supports' =>array(
                'title','revisions','page-attributes','thumbnail'
            )
        ));
    }

    add_action('init','office_master_custom_post');

include_once('inc\cmb2-custom-field.php');