<?php

function pallab_theme_init(){

    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');

    add_image_size('post_thums', 600, 400, false);

    register_nav_menus(array(
        'header_menu'  =>  'Main Menu',
        'footer_menu' =>   'Bottom Menu',
        'primary_menu' =>  'Primary_menu'
    ));
}
 add_action('after_setup_theme','pallab_theme_init');


function pallab_css_js(){

    wp_enqueue_style('main-style',get_stylesheet_uri(),array('pallab_test_style'),'v142.25','all');
    wp_enqueue_style('pallab_test_style',get_template_directory_uri().'/css/test.css','null','v1.2','all');

    wp_enqueue_script('pallab_test_script',get_template_directory_uri().'/js/java.js',array('jquery'),'v12.2',true);
    wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts','pallab_css_js');