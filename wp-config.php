<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pallab');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vA930yYz|W/Hp`1px8l}H!w}/f)zxC$@GylW`6zXw>Orj641FC;[$}Et -;5|] 2');
define('SECURE_AUTH_KEY',  'H=7YWJXp=9~>f8b_1S7(I(Vf!ANo6Pu4UO{rjwSp59C c^aAnu[M;Q]B_ptatr)c');
define('LOGGED_IN_KEY',    'w5(8<:Vu;,vN:HPU:9l`d28wIR2P&$PN$%mJ<-$kX~#YcMU#r3A)3th[Qajt (P:');
define('NONCE_KEY',        'C4z]tEB5U !_u,w8zDs^MGYiN8mJ!;^0^z?h}kVBML5x(UQvH1${b]*PoYJB&[xu');
define('AUTH_SALT',        '%<tSRq2*2(+u}zTb3H$Fn7ZdxqK^o[[d17=^T^K$N|Me-(qI9I+NY%o4=98E}ra?');
define('SECURE_AUTH_SALT', 'As=w5vw(lS}Q:)c&M~gj0Umhx$5-LbUx13tfw]PqZ{j~Gm0mi%SBs(c^%5;I*S%^');
define('LOGGED_IN_SALT',   '*<#wVHAA-hA]z`A]+v7W1On%WMJx7|S_yVw LB][ste68@m%:+c+u2J,Mq np*Sv');
define('NONCE_SALT',       'p ~UFaSq)$O8_~F6:!ovv``%OY%fC0o yf1]8:wHU-=r]8JR:#3xIneK*roXx>*3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pallabwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
